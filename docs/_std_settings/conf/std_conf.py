# Copyright (C) ALbert Mietus, 2015,2019 -- Copy when you like!!
#  STANDARD CONFiguration for Sphinx-doc
# -*- coding: utf-8 -*-


### Some setting depend on RTD
import os
on_rtd = os.environ.get('READTHEDOCS') == 'True'


###
### File/Project layout
###

master_doc        = 'index'
source_suffix     = '.rst'
exclude_patterns  = ['**/.#*', '**/_*']
html_static_path  = ['../_std_settings/static/',    '_static']
templates_path    = ['../_std_settings/templates/', '_templates' ]
html_css_files    = ['TechPush.css', 'local_custom.css'] # local_custom.css is for project specific custum style ; default, the file is empty


###
### Kick off
###
try:    extensions = extensions
except NameError: extensions=[]

show_authors = True

rst_epilog = None
rst_prolog = """
.. include:: /_generic.inc

"""

if not on_rtd:
    html_sidebars = { '**': [
        'globaltoc.html',
        'searchbox.html',
        'relations.html',
        'sourcelink.html',
    ]}

if not on_rtd:
    html_theme = 'classic' # XXX This will change  --GAM


# sphinx.ext.todo
#-----------------
extensions.append('sphinx.ext.todo')
todo_include_todos=True


# sphinx.ext.autodoc, sphinxcontrib.napoleon
#-------------------------------------------

extensions.append('sphinx.ext.autodoc')
extensions.append('sphinxcontrib.napoleon') # For google- & NumPy-style docstrings

autodoc_default_flags     = ['members', 'show-inheritance']
autoclass_content         = "both"
autodoc_member_order      = "bysource"

napoleon_google_docstring = True
napoleon_numpy_docstring  = False
napoleon_use_ivar         = True
napoleon_use_rtype        = True


# plantUML
#---------
extensions.append('sphinxcontrib.plantuml')
plantuml = 'plantuml'


# Needs
#------
extensions.append('sphinxcontrib.needs')
needs_include_needs = True
needs_id_required = True
needs_id_regex = r'^[A-Z][A-Za-z-0-9_]{4,}'

needs_types = [
    dict(directive="demo", title="Demonstrator",   prefix="D_", color="#BFD8D2", style="node"),
    dict(directive="req",  title="Requirement",    prefix="R_", color="#BFD8D2", style="node"),
    dict(directive="spec", title="Specification",  prefix="S_", color="#FEDCD2", style="node"),
    dict(directive="impl", title="Implementation", prefix="I_", color="#DF744A", style="node"),
    dict(directive="test", title="Test Case",      prefix="T_", color="#DCB239", style="node")
]

needs_statuses = [
    dict(name="idea", description="Just an idea; not accepted, no time should be spend on it"),
    dict(name="import", description="Some work is done, but not here (counts as 'open')"),
    dict(name="open", description="Nothing done yet"),
    dict(name="in progress", description="Someone is working on it"),
    dict(name="implemented", description="Work is done and tested"),
    dict(name="on-hold", description="Work (if any) is stopped until further notice"),
]
